#!/bin/sh

if [ "$1" != --compile ]
then
    exec devroot-enter \
        "$1" \
        --bind-ro $(readlink -f $0):/tmp/cross-compile.sh \
        /tmp/cross-compile.sh --compile
fi

sudo apt update

if apt source --compile dash
then
    echo sdk-cross-compilation: pass
    exit 0
else
    echo sdk-cross-compilation: fail
    exit 1
fi
